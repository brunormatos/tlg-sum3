//Library includes
#include <Windows.h>
#include <cassert>
#include <vld.h>
#include <thread>
#include <functional>
#include <signal.h>
#include <iostream>
#include <algorithm>	// std::replace

//Local includes
#include "network.h"
#include "logger.h"
#include "inputlinebuffer.h"
#include "consoletools.h"
#include "server.h"
#include "client.h"

//Static link to winsock lib
#pragma comment(lib,"ws2_32.lib")


std::string g_sFileName = "";
char* _pcPacketData = 0; //A global buffer to receive packet data info
CInputLineBuffer * _InputBuffer = nullptr;
std::thread _ClientReceiveThread, _ServerReceiveThread;
////A pointer to hold a client instance
CClient* _pClient = nullptr;
//A pointer to hold a server instance
CServer* _pServer = nullptr;
bool g_bIsTerminating = false;

//Destroy every pointer initialized with the application
void TerminateApp()
{
	CNetwork& _rNetwork = CNetwork::GetInstance();
	TPacket _packet;
	_packet.Serialize(SHUTDOWN, const_cast<char*>("")); //Hardcoded username; change to name as taken in via user input.
	if (_rNetwork.GetNetworkEntity()->SendData(_packet.PacketData))
	{
		myLog("Sent closing message\n");
	}

	_rNetwork.SetToOffline();

	std::string applicationType = "";
	
	if (_pServer != nullptr) applicationType = "server";
	else if (_pClient != nullptr) applicationType = "client";

	myLog("Trying to join my thread.\n");
	if (_ClientReceiveThread.joinable()) _ClientReceiveThread.join();
	if (_ServerReceiveThread.joinable()) _ServerReceiveThread.join();
	//std::this_thread::sleep_for(std::chrono::seconds(30));

	myLog("Joined my thread.\n");

	_rNetwork.ShutDown();
	_rNetwork.DestroyInstance();


	delete _InputBuffer;
	delete[] _pcPacketData;
	myLog("Reached the end of the " + applicationType + " application.\n");
}

BOOL WINAPI ConsoleHandler(DWORD dwCtrlEvent)
{
	switch (dwCtrlEvent)
	{
	case CTRL_C_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
	{
		g_bIsTerminating = true;
		myLog("New termination.\n");
		TerminateApp();
		myLog("Finished new termination.\n");
		delete CLogger::GetInstance();
		return TRUE;
	}

	default:
		/*g_bIsTerminating = true;
		myLog("New termination.\n");
		TerminateApp();
		myLog("Finished new termination.\n");
		delete CLogger::GetInstance();*/
		return FALSE;
	}
}

//setting up signal handlers for when the application should terminate
void SigInt_Handler(int n_signal)
{
	myLog("interrupted\n");
	TerminateApp();
	delete CLogger::GetInstance();
	exit(1);
}

void SigBreak_Handler(int n_signal)
{
	myLog("Started closing\n");
	TerminateApp();
	myLog("Finished closing\n");
	delete CLogger::GetInstance();
	exit(2);
}



int main()
{
	//signal(SIGINT, &SigInt_Handler);
	//signal(SIGBREAK, &SigBreak_Handler);

	if (SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler, TRUE) == FALSE)
	{
		// unable to install handler... 
		// display message to the user
		myLog("Unable to install handler!\n");

			//save file;
	}

	g_sFileName = CLogger::getCurrentDateTime("now");
	std::replace(g_sFileName.begin(), g_sFileName.end(), ':', '-');
	std::replace(g_sFileName.begin(), g_sFileName.end(), '_', '-');
	std::replace(g_sFileName.begin(), g_sFileName.end(), ' ', '_');
	g_sFileName = "UDPChat_Log_" + g_sFileName;
	myLogOpen(g_sFileName, "Started UDP Chat application.");

	_pcPacketData = new char[MAX_MESSAGE_LENGTH];
	strcpy_s(_pcPacketData, strlen("") + 1, "");

	char _cIPAddress[MAX_ADDRESS_LENGTH]; // An array to hold the IP Address as a string
										  //ZeroMemory(&_cIPAddress, strlen(_cIPAddress));
	unsigned char _ucChoice;
	EEntityType _eNetworkEntityType;
	_InputBuffer = new CInputLineBuffer(MAX_MESSAGE_LENGTH);
	
	//Get the instance of the network
	CNetwork& _rNetwork = CNetwork::GetInstance();
	_rNetwork.StartUp();

	// query, is this to be a client or a server?
	_ucChoice = QueryOption("Do you want to run a client or server (C/S)?", "CS");
	switch (_ucChoice)
	{
	case 'C':
	{
		_eNetworkEntityType = CLIENT;
		break;
	}
	case 'S':
	{
		_eNetworkEntityType = SERVER;
		break;
	}
	default:
	{
		std::cout << "This is not a valid option" << std::endl;
		return 0;
		break;
	}
	}
	if (!_rNetwork.Initialise(_eNetworkEntityType))
	{
		std::cout << "Unable to initialize the Network........Press any key to continue......";
		_getch();
		return 0;
	}

	//Run receive on a separate thread so that it does not block the main client thread.
	if (_eNetworkEntityType == CLIENT) //if network entity is a client
	{
		_pClient = static_cast<CClient*>(_rNetwork.GetNetworkEntity());
		_ClientReceiveThread = std::thread(&CClient::ReceiveData, _pClient, std::ref(_pcPacketData));
		myLog("Initiated client socket and client receiving thread\n");
	}

	//Run receive of server also on a separate thread 
	else if (_eNetworkEntityType == SERVER) //if network entity is a server
	{
		_pServer = static_cast<CServer*>(_rNetwork.GetNetworkEntity());
		_ServerReceiveThread = std::thread(&CServer::ReceiveData, _pServer, std::ref(_pcPacketData));
		myLog("Initiated server socket and server receiving thread\n");
	}

	while (_rNetwork.IsOnline())
	{
		if (_eNetworkEntityType == CLIENT) //if network entity is a client
		{
			_pClient = static_cast<CClient*>(_rNetwork.GetNetworkEntity());

			//Prepare for reading input from the user
			_InputBuffer->PrintToScreenTop();

			//Get input from the user
			if (_InputBuffer->Update())
			{
				ECommandType command = _InputBuffer->IsACommand();
				if (command != ECommandType::NONE)
				{
					TPacket _packet;
					switch (command)
					{
					case ECommandType::HELP:
						std::cout << "List of Commands:" << std::endl;
						std::cout << "[!?] - displays a list with all commands available" << std::endl;
						std::cout << "[!q] - quits the application" << std::endl;
						std::cout << "[!!] - bang" << std::endl;
						std::cout << "[!u] - displays a list with all connected users" << std::endl;
						std::cout << "[!s {user_name} {your message goes here}] - send secret message to user" << std::endl;
						break;
					case ECommandType::QUIT:
						_packet.Serialize(SHUTDOWN, const_cast<char*>(""));
						_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);

						_pClient->GetWorkQueue()->empty();
						_rNetwork.GetInstance().SetToOffline();
						break;
					case ECommandType::USER_LIST:
						_packet.Serialize(USERS, const_cast<char*>(""));
						_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);
						break;
					case ECommandType::NONE:
					default:
						break;
					}

					//Clear the Input Buffer
					_InputBuffer->ClearString();
					//Print To Screen Top
					_InputBuffer->PrintToScreenTop();
				}
				else
				{
					// we completed a message, lets send it:
					int _iMessageSize = static_cast<int>(strlen(_InputBuffer->GetString()));

					//Put the message into a packet structure
					TPacket _packet;
					_packet.Serialize(DATA, const_cast<char*>(_InputBuffer->GetString())); //Hardcoded username; change to name as taken in via user input.
					_rNetwork.GetNetworkEntity()->SendData(_packet.PacketData);
					//Clear the Input Buffer
					_InputBuffer->ClearString();
					//Print To Screen Top
					_InputBuffer->PrintToScreenTop();
				}
			}
			if (_pClient != nullptr)
			{
				//If the message queue is empty 
				if (_pClient->GetWorkQueue()->empty())
				{
					//Don't do anything
				}
				else
				{
					//Retrieve off a message from the queue and process it
					std::string temp;
					_pClient->GetWorkQueue()->pop(temp);
					_pClient->ProcessData(const_cast<char*>(temp.c_str()));
				}
			}

		}
		else //if you are running a server instance
		{
			//_InputBuffer->PrintToScreenTop();

			//Get input from the user
			if (_InputBuffer->Update())
			{
				//_InputBuffer->PrintToScreenTop();
				const char * buffer = _InputBuffer->GetString();
				if (buffer[0] == '!')
				{
					if (buffer[1] == 'q')
					{
						_rNetwork.SetToOffline();
						_InputBuffer->ClearString();
						_InputBuffer->PrintToScreenTop();
						break;
					}
				}
				//Clear the Input Buffer*/
				_InputBuffer->ClearString();
				//Print To Screen Top
				_InputBuffer->PrintToScreenTop();
			}
			if (_pServer != nullptr)
			{
				if (!_pServer->GetWorkQueue()->empty())
				{
					_rNetwork.GetNetworkEntity()->GetRemoteIPAddress(_cIPAddress);
					//std::cout << _cIPAddress
					//<< ":" << _rNetwork.GetInstance().GetNetworkEntity()->GetRemotePort() << "> " << _pcPacketData << std::endl;

					//Retrieve off a message from the queue and process it
					_pServer->GetWorkQueue()->pop(_pcPacketData);
					_pServer->ProcessData(_pcPacketData);
				}
			}
		}


	} //End of while network is Online*/
	
	//if (_pClient == nullptr)
	//{
		//int Temp;
		//std::cin >> Temp;
	//}

	if(!g_bIsTerminating)
	{
		TerminateApp();
		delete CLogger::GetInstance();
	}
	return 0;
}