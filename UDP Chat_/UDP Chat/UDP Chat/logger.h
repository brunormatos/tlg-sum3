#ifndef LOGGER_H_INCLUDED
#define LOGGER_H_INCLUDED

#pragma once

//library includes
#include <string>

#define myLogOpen(nameFile, s) CLogger::Logger(nameFile, s);
#define myLog(s) CLogger::Logger(s);


class CLogger
{
public:
	static std::string getCurrentDateTime(std::string);
	static void Logger(std::string, std::string);
	static void Logger(std::string);
	//static void DestroyInstance();

	CLogger();
	~CLogger() { m_pInstance = nullptr; }

	static CLogger* GetInstance()
	{
		if (m_pInstance == nullptr)
			m_pInstance = new CLogger();
		return m_pInstance;
	}

private:
	static CLogger* m_pInstance;
	std::string m_sFilePath;
};


#endif //LOGGER_H_INCLUDED