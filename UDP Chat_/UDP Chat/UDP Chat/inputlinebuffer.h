#ifndef INPUTLINEBUFFER_H_INCLUDED
#define INPUTLINEBUFFER_H_INCLUDED

enum ECommandType : unsigned short
{
	BANG,
	QUIT,
	HELP,
	USER_LIST,
	NONE
};

class CInputLineBuffer
{
public:
	CInputLineBuffer(unsigned int uBufferSize);
	~CInputLineBuffer();
	
	void ClearString();
	bool Update();
	void PrintToScreenTop();
	const char* GetString() const;
	ECommandType IsACommand();

protected:
	unsigned int	m_uBufferSize;	//!< The total size of the buffer.
	unsigned int	m_uBufferPos;	//!< The position of the next char in the buffer to be entered by the user.
	char*			m_pBuffer;		//!< The buffer contents.
};

#endif //INPUTLINEBUFFER_H_INCLUDED
