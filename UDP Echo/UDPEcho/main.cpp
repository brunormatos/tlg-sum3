//
// Bachelor of Software Engineering
// Media Design School
// Auckland
// New Zealand
//
// (c) 2018 Media Design School
//
// File Name	: 
// Description	: 
// Author		: Your Name
// Mail			: your.name@mediadesign.school.nz
//

//Library Includes
#include <Windows.h>
#include <cassert>
#include <vld.h>
#include <thread>
#include <iostream>
#include <signal.h>

//Local Includes
#include "consoletools.h"
#include "network.h"
#include "client.h"
#include "server.h"
#include "InputLineBuffer.h"
#include <functional>

// make sure the winsock lib is included...
#pragma comment(lib,"ws2_32.lib")


char* _pcPacketData = 0;
std::thread _ClientReceiveThread, _ServerReceiveThread;
//A pointer to hold a client instance
CClient* _pClient = nullptr;
//A pointer to hold a server instance
CServer* _pServer = nullptr;


void SigInt_Handler(int n_signal)
{
	OutputDebugString("interrupted\n");

	CNetwork& _rNetwork = CNetwork::GetInstance();
	if (_rNetwork.IsOnline())
		_rNetwork.SetToOffline();
	if (_pClient != nullptr)
	{
		if (_ClientReceiveThread.joinable())
			_ClientReceiveThread.join();
		else
			std::cout << "Thread not joinable." << std::endl;
	}

	if (_pServer != nullptr)
	{
		std::cout << "Thread not joined." << std::endl;
		_ServerReceiveThread.join();
		std::cout << "Thread join." << std::endl;
	}

	//Shut Down the Network
	_rNetwork.ShutDown();
	_rNetwork.DestroyInstance();

	delete[] _pcPacketData;

	exit(1);
}

void SigBreak_Handler(int n_signal)
{
	OutputDebugString("closed\n");

	CNetwork& _rNetwork = CNetwork::GetInstance();
	if (_pClient != nullptr)
	{
		TPacket _packet;
		_packet.Serialize(KILL, const_cast<char*>(""));
		_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);

		_pClient->GetWorkQueue()->empty();
		_rNetwork.GetInstance().SetToOffline();

		/*if (_ClientReceiveThread.joinable())
			_ClientReceiveThread.join();
		else
			std::cout << "Thread not joinable." << std::endl;*/
	}

	if (_pServer != nullptr)
	{
		OutputDebugString("Thread not joined.\n");
		_ServerReceiveThread.join();
		OutputDebugString("Thread join.\n");
		//Shut Down the Network
		_rNetwork.ShutDown();
		_rNetwork.DestroyInstance();

		delete[] _pcPacketData;
		exit(2);
	}

}


int main()
{
	signal(SIGINT, &SigInt_Handler);
	signal(SIGBREAK, &SigBreak_Handler);

	//char* _pcPacketData = 0; //A local buffer to receive packet data info
	_pcPacketData = new char[MAX_MESSAGE_LENGTH];
	strcpy_s(_pcPacketData, strlen("") + 1, "");

	char _cIPAddress[MAX_ADDRESS_LENGTH]; // An array to hold the IP Address as a string
										  //ZeroMemory(&_cIPAddress, strlen(_cIPAddress));
	
	unsigned char _ucChoice;
	EEntityType _eNetworkEntityType;
	CInputLineBuffer _InputBuffer(MAX_MESSAGE_LENGTH);
	//std::thread _ClientReceiveThread, _ServerReceiveThread;
	
	//Get the instance of the network
	CNetwork& _rNetwork = CNetwork::GetInstance();
	_rNetwork.StartUp();
	
	////A pointer to hold a client instance
	//CClient* _pClient = nullptr;
	////A pointer to hold a server instance
	//CServer* _pServer = nullptr;
	
	// query, is this to be a client or a server?
	_ucChoice = QueryOption("Do you want to run a client or server (C/S)?", "CS");
	switch (_ucChoice)
	{
	case 'C':
	{
		_eNetworkEntityType = CLIENT;
		break;
	}
	case 'S':
	{
		_eNetworkEntityType = SERVER;
		break;
	}
	default:
	{
		std::cout << "This is not a valid option" << std::endl;
		return 0;
		break;
	}
	}
	if (!_rNetwork.GetInstance().Initialise(_eNetworkEntityType))
	{
		std::cout << "Unable to initialise the Network........Press any key to continue......";
		_getch();
		return 0;
	}
	
	//Run receive on a separate thread so that it does not block the main client thread.
	if (_eNetworkEntityType == CLIENT) //if network entity is a client
	{

		_pClient = static_cast<CClient*>(_rNetwork.GetInstance().GetNetworkEntity());
		_ClientReceiveThread = std::thread(&CClient::ReceiveData, _pClient, std::ref(_pcPacketData));

	}

	//Run receive of server also on a separate thread 
	else if (_eNetworkEntityType == SERVER) //if network entity is a server
	{

		_pServer = static_cast<CServer*>(_rNetwork.GetInstance().GetNetworkEntity());
		_ServerReceiveThread = std::thread(&CServer::ReceiveData, _pServer, std::ref(_pcPacketData));

	}
	
	while (_rNetwork.IsOnline())
	{
		if (_eNetworkEntityType == CLIENT) //if network entity is a client
		{
			_pClient = static_cast<CClient*>(_rNetwork.GetInstance().GetNetworkEntity());

			//Prepare for reading input from the user
			_InputBuffer.PrintToScreenTop();

			//Get input from the user
			if (_InputBuffer.Update())
			{
				ECommandType command = _InputBuffer.IsACommand();
				if (command != ECommandType::NONE)
				{
					TPacket _packet;
					switch (command)
					{
					case ECommandType::HELP:
						std::cout << "List of Commands:" << std::endl;
						std::cout << "[!?] - displays a list with all commands available" << std::endl;
						std::cout << "[!q] - quits the application" << std::endl;
						std::cout << "[!!] - bang" << std::endl;
						std::cout << "[!u] - displays a list with all connected users" << std::endl;
						std::cout << "[!s {user_name} {your message goes here}] - send secret message to user" << std::endl;
						break;
					case ECommandType::QUIT:
						_packet.Serialize(KILL, const_cast<char*>(""));
						_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);

						_pClient->GetWorkQueue()->empty();
						_rNetwork.GetInstance().SetToOffline();
						break;
					case ECommandType::USER_LIST:
						_packet.Serialize(USERS, const_cast<char*>(""));
						_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);
						break;
					case ECommandType::NONE:
					default:
						break;
					}

					//Clear the Input Buffer
					_InputBuffer.ClearString();
					//Print To Screen Top
					_InputBuffer.PrintToScreenTop();
				}
				else
				{
					// we completed a message, lets send it:
					int _iMessageSize = static_cast<int>(strlen(_InputBuffer.GetString()));

					//Put the message into a packet structure
					TPacket _packet;
					_packet.Serialize(DATA, const_cast<char*>(_InputBuffer.GetString())); //Hardcoded username; change to name as taken in via user input.
					_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);
					//Clear the Input Buffer
					_InputBuffer.ClearString();
					//Print To Screen Top
					_InputBuffer.PrintToScreenTop();
				}
			}
			if (_pClient != nullptr)
			{
				//If the message queue is empty 
				if (_pClient->GetWorkQueue()->empty())
				{
					//Don't do anything
				}
				else
				{
					//Retrieve off a message from the queue and process it
					_pClient->GetWorkQueue()->pop(_pcPacketData);
					_pClient->ProcessData(_pcPacketData);
				}
			}

		}
		else //if you are running a server instance
		{

			if (_pServer != nullptr)
			{
				if (!_pServer->GetWorkQueue()->empty())
				{
					_rNetwork.GetInstance().GetNetworkEntity()->GetRemoteIPAddress(_cIPAddress);
					//std::cout << _cIPAddress
					//<< ":" << _rNetwork.GetInstance().GetNetworkEntity()->GetRemotePort() << "> " << _pcPacketData << std::endl;

					//Retrieve off a message from the queue and process it
					_pServer->GetWorkQueue()->pop(_pcPacketData);
					_pServer->ProcessData(_pcPacketData);
				}
			}
		}


	} //End of while network is Online
	
	//_rNetwork.SetToOffline();
	
	if (_pClient != nullptr)
	{
		if(_ClientReceiveThread.joinable())
			_ClientReceiveThread.join();
		else
			std::cout << "Thread not joinable." << std::endl;
	}

	if (_pServer != nullptr)
	{
		std::cout << "Thread not joined." << std::endl;
		_ServerReceiveThread.join();
		std::cout << "Thread join." << std::endl;
	}
	
	//Shut Down the Network
	_rNetwork.ShutDown();
	_rNetwork.DestroyInstance();

	delete[] _pcPacketData;
	return 0;
}