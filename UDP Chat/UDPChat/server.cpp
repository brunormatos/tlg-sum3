/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: server.cpp
Description	: source for the UDP server application of our chat service
Author		: Bruno Matos
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/

//Library Includes
#include <WS2tcpip.h>
#include <iostream>
#include <utility>
#include <thread>
#include <chrono>


//Local Includes
#include "utils.h"
#include "network.h"
#include "consoletools.h"
#include "socket.h"


//Local Includes
#include "server.h"

CServer::CServer()
	:m_pcPacketData(0),
	m_pServerSocket(0)
{
	ZeroMemory(&m_ClientAddress, sizeof(m_ClientAddress));
}

CServer::~CServer()
{
	delete m_pConnectedClients;
	m_pConnectedClients = 0;

	delete m_pServerSocket;
	m_pServerSocket = 0;

	while (!m_pWorkQueue->empty()) {
		char * item;
		m_pWorkQueue->pop(item);
	}
	delete m_pWorkQueue;
	m_pWorkQueue = 0;
	
	delete[] m_pcPacketData;
	m_pcPacketData = 0;
}

bool CServer::Initialise()
{
	m_pcPacketData = new char[MAX_MESSAGE_LENGTH];
	
	//Create a work queue to distribute messages between the main  thread and the receive thread.
	m_pWorkQueue = new CWorkQueue<char*>();

	//Create a socket object
	m_pServerSocket = new CSocket();

	//Get the port number to bind the socket to
	unsigned short _usServerPort = QueryPortNumber(DEFAULT_SERVER_PORT);

	//Initialise the socket to the local loop back address and port number
	if (!m_pServerSocket->Initialise(_usServerPort))
	{
		return false;
	}

	//Qs 2: Create the map to hold details of all connected clients
	m_pConnectedClients = new std::map < std::string, TClientDetails >() ;

	return true;
}

bool CServer::AddClient(std::string _strClientName)
{
	//TO DO : Add the code to add a client to the map here...
	
	for (auto it = m_pConnectedClients->begin(); it != m_pConnectedClients->end(); ++it)
	{
		//Check to see that the client to be added does not already exist in the map, 
		if(it->first == ToString(m_ClientAddress))
		{
			return false;
		}
		//also check for the existence of the username
		else if (it->second.m_strName == _strClientName)
		{
			TPacket _packet;
			_packet.Serialize(HANDSHAKE, "");
			SendData(_packet.PacketData);
			return false;
		}
	}
	//Add the client to the map.
	TClientDetails _clientToAdd;
	_clientToAdd.m_strName = _strClientName;
	_clientToAdd.m_ClientAddress = this->m_ClientAddress;

	std::string _strAddress = ToString(m_ClientAddress);
	m_pConnectedClients->insert(std::pair < std::string, TClientDetails > (_strAddress, _clientToAdd));
	return true;
}

bool CServer::IsUserConnected()
{
	for (auto it = m_pConnectedClients->begin(); it != m_pConnectedClients->end(); it++)
	{
		if (it->first == ToString(m_ClientAddress))
		{
			return true;
		}
	}
	return false;
}

bool CServer::SendData(char* _pcDataToSend)
{
	int _iBytesToSend = (int)strlen(_pcDataToSend) + 1;
	
	int iNumBytes = sendto(
		m_pServerSocket->GetSocketHandle(),				// socket to send through.
		_pcDataToSend,									// data to send
		_iBytesToSend,									// number of bytes to send
		0,												// flags
		reinterpret_cast<sockaddr*>(&m_ClientAddress),	// address to be filled with packet target
		sizeof(m_ClientAddress)							// size of the above address struct.
		);
	//iNumBytes;
	if (_iBytesToSend != iNumBytes)
	{
		std::cout << "There was an error in sending data from client to server" << std::endl;
		return false;
	}
	return true;
}

bool CServer::SendDataToUser(char* _pcDataToSend, std::string _username)
{
	int _iBytesToSend = (int)strlen(_pcDataToSend) + 1;

	std::map<std::string, TClientDetails>::iterator client;

	for (client = m_pConnectedClients->begin(); client != m_pConnectedClients->end(); client++)
	{
		if(client->second.m_strName == _username)
		{
			int _iBytesToSend = (int)strlen(_pcDataToSend) + 1;

			int iNumBytes = sendto(
				m_pServerSocket->GetSocketHandle(),				// socket to send through.
				_pcDataToSend,									// data to send
				_iBytesToSend,									// number of bytes to send
				0,												// flags
				(sockaddr *)& client->second.m_ClientAddress,	// address to be filled with packet target
				sizeof(client->second.m_ClientAddress)							// size of the above address struct.
			);
			//iNumBytes;
			if (_iBytesToSend != iNumBytes)
			{
				std::cout << "There was an error in sending data from client to server" << std::endl;
				return false;
			}
			break;
		}
	}

	return true;
}

bool CServer::BroadcastData(char* _pcDataToSend)
{
	int _iBytesToSend = (int)strlen(_pcDataToSend) + 1;

	std::map<std::string, TClientDetails>::iterator client;

	for (client = m_pConnectedClients->begin(); client != m_pConnectedClients->end(); client++)
	{
		int _iBytesToSend = (int)strlen(_pcDataToSend) + 1;

		int iNumBytes = sendto(
			m_pServerSocket->GetSocketHandle(),				// socket to send through.
			_pcDataToSend,									// data to send
			_iBytesToSend,									// number of bytes to send
			0,												// flags
			(sockaddr *)& client->second.m_ClientAddress,	// address to be filled with packet target
			sizeof(client->second.m_ClientAddress)							// size of the above address struct.
		);
		//iNumBytes;
		if (_iBytesToSend != iNumBytes)
		{
			std::cout << "There was an error in sending data from client to server" << std::endl;
			return false;
		}
	}

	return true;
}

void CServer::ReceiveData(char* _pcBufferToReceiveData)
{
	
	int iSizeOfAdd = sizeof(m_ClientAddress);
	int _iNumOfBytesReceived;
	int _iPacketSize;

	//Make a thread local buffer to receive data into
	char _buffer[MAX_MESSAGE_LENGTH];

	while (m_bOnline)
	{
		// pull off the packet(s) using recvfrom()
		_iNumOfBytesReceived = recvfrom(			// pulls a packet from a single source...
			m_pServerSocket->GetSocketHandle(),						// client-end socket being used to read from
			_buffer,							// incoming packet to be filled
			MAX_MESSAGE_LENGTH,					   // length of incoming packet to be filled
			0,										// flags
			reinterpret_cast<sockaddr*>(&m_ClientAddress),	// address to be filled with packet source
			&iSizeOfAdd								// size of the above address struct.
		);
		if (_iNumOfBytesReceived < 0)
		{
			int _iError = WSAGetLastError();
			//ErrorRoutines::PrintWSAErrorInfo(_iError);
			if (WSAEINTR == _iError)
			{

			}
			if (WSAECONNRESET == _iError)	// replies to the keep-alive messages being sent through the keep-alive thread
			{
				myLog("The client at " + ToString(m_ClientAddress) + " has dropped his connection.\n");
				std::string username = "";
				for (auto it = m_pConnectedClients->begin(); it != m_pConnectedClients->end(); it++)
				{
					if (it->first == ToString(m_ClientAddress))
					{
						username = it->second.m_strName;
						m_pConnectedClients->erase(it++);
						break;
					}
				}
				TPacket _packet;
				_packet.Serialize(USER_LEFT, (char*)username.c_str());
				BroadcastData(_packet.PacketData);
			}
			//return false;
		}
		else
		{
			_iPacketSize = static_cast<int>(strlen(_buffer)) + 1;
			strcpy_s(_pcBufferToReceiveData, _iPacketSize, _buffer);
			char _IPAddress[100];
			inet_ntop(AF_INET, &m_ClientAddress.sin_addr, _IPAddress, sizeof(_IPAddress));
			
			std::cout << "Server Received \"" << _pcBufferToReceiveData << "\" from " <<
				_IPAddress << ":" << ntohs(m_ClientAddress.sin_port) << std::endl;
			//Push this packet data into the WorkQ
			m_pWorkQueue->push(_pcBufferToReceiveData);
		}
		//std::this_thread::yield();
		
	} //End of while (true)
}

void CServer::GetRemoteIPAddress(char *_pcSendersIP)
{
	char _temp[MAX_ADDRESS_LENGTH];
	int _iAddressLength;
	inet_ntop(AF_INET, &(m_ClientAddress.sin_addr), _temp, sizeof(_temp));
	_iAddressLength = static_cast<int>(strlen(_temp)) + 1;
	strcpy_s(_pcSendersIP, _iAddressLength, _temp);
}

unsigned short CServer::GetRemotePort()
{
	return ntohs(m_ClientAddress.sin_port);
}

void CServer::ProcessData(char* _pcDataReceived)
{
	TPacket _packetRecvd, _packetToSend;
	_packetRecvd = _packetRecvd.Deserialize(_pcDataReceived);
	switch (_packetRecvd.MessageType)
	{
	case HANDSHAKE:
	{
		std::cout << "Server received a handshake message " << std::endl;
		if (AddClient(_packetRecvd.MessageContent))
		{
			//Qs 3: To DO : Add the code to do a handshake here
			char* msg = new char[MAX_MESSAGE_LENGTH];
			std::string users("");

			sprintf_s(msg, MAX_MESSAGE_LENGTH, "%s", _packetRecvd.MessageContent);
			_packetToSend.Serialize(NEW_USER, msg);
			BroadcastData(_packetToSend.PacketData);

			std::cout << "Sending a list of connected usernames." << std::endl;
			for (auto it = m_pConnectedClients->begin(); it != m_pConnectedClients->end(); ++it)
			{
				users.append(it->second.m_strName);
				users.append("|");
			}

			if (users.length() > 0) {
				std::string::iterator it = users.end() - 1;
				if (*it == '|') {
					users.erase(it);
				}
			}

			sprintf_s(msg, MAX_MESSAGE_LENGTH, users.c_str());
			//TPacket _packet;
			_packetToSend.Serialize(USERS, msg);
			SendData(_packetToSend.PacketData);

			//sprintf_s(msg, MAX_MESSAGE_LENGTH, std::string("Enter !? for list of commands").c_str());
			//TPacket _packet;
			_packetToSend.Serialize(COMMAND, "");
			SendData(_packetToSend.PacketData);

			delete[] msg;
		}
		break;
	}
	case DATA:
	{
		if (!IsUserConnected())
		{
			_packetToSend.Serialize(MERROR, (char*) "Operation not authorized. You must connect first.");
			SendData(_packetToSend.PacketData);
		}
		else
		{
			char* msg = new char[MAX_MESSAGE_LENGTH];
			std::string msgString;
			for (auto it = m_pConnectedClients->begin(); it != m_pConnectedClients->end(); ++it)
			{
				if (ToString(it->second.m_ClientAddress) == ToString(m_ClientAddress)) {
					msgString = it->second.m_strName + ">" + _packetRecvd.MessageContent;
					break;
				}
			}
			sprintf_s(msg, MAX_MESSAGE_LENGTH, msgString.c_str());
			_packetToSend.Serialize(DATA, msg);
			BroadcastData(_packetToSend.PacketData);
			delete[] msg;
		}
		break;
	}

	case BROADCAST:
	{
		std::cout << "Received a broadcast packet" << std::endl;
		//Just send out a packet to the back to the client again which will have the server's IP and port in it's sender fields
		_packetToSend.Serialize(BROADCAST, "I'm here!");
		SendData(_packetToSend.PacketData);
		break;
	}

	case PRIVATE:
	{
		if (!IsUserConnected())
		{
			_packetToSend.Serialize(MERROR, (char*) "Operation not authorized. You must connect first.");
			SendData(_packetToSend.PacketData);
		}
		else
		{
			char* msg = new char[MAX_MESSAGE_LENGTH];
			std::cout << "Received " << _packetRecvd.MessageContent << std::endl;
			std::string msgReceived(_packetRecvd.MessageContent);
			std::string destUsername = msgReceived.substr(0, msgReceived.find(" "));
			if (int pos = msgReceived.find(" "))
				msgReceived = msgReceived.substr(pos + 1);
			else msgReceived = "";
			//build the message
			std::string msgString;
			for (auto it = m_pConnectedClients->begin(); it != m_pConnectedClients->end(); ++it)
			{
				if (ToString(it->second.m_ClientAddress) == ToString(m_ClientAddress)) {
					msgString = it->second.m_strName + "::" + destUsername + ">" + msgReceived;
					break;
				}
			}
			sprintf_s(msg, MAX_MESSAGE_LENGTH, msgString.c_str());
			_packetToSend.Serialize(DATA, msg);
			//SendData(_packetSent.PacketData);
			SendData(_packetToSend.PacketData);
			SendDataToUser(_packetToSend.PacketData, destUsername);
			//std::this_thread::sleep_for(std::chrono::milliseconds(50));

			//_packetSent.Serialize(DATA, "TEST MESSAGE");
			//SendData(_packetSent.PacketData);
			delete[] msg;
			break;
		}
	}

	case USERS:
	{
		if (!IsUserConnected())
		{
			_packetToSend.Serialize(MERROR, (char*) "Operation not authorized. You must connect first.");
			SendData(_packetToSend.PacketData);
		}
		else
		{
			char* msg = new char[MAX_MESSAGE_LENGTH];
			std::string users("");

			std::cout << "Sending a list of connected usernames." << std::endl;
			for (auto it = m_pConnectedClients->begin(); it != m_pConnectedClients->end(); ++it)
			{
				users.append(it->second.m_strName);
				users.append("|");
			}

			if (users.length() > 0) {
				std::string::iterator it = users.end() - 1;
				if (*it == '|') {
					users.erase(it++);
				}
			}

			sprintf_s(msg, MAX_MESSAGE_LENGTH, users.c_str());
			//TPacket _packet;
			_packetToSend.Serialize(USERS, msg);
			SendData(_packetToSend.PacketData);

			delete[] msg;
		}
		break;
	}

	default:
		if (!IsUserConnected())
		{
			_packetToSend.Serialize(MERROR, (char*) "Operation not authorized. You must connect first.");
			SendData(_packetToSend.PacketData);
		}
		break;

	}
}

CWorkQueue<char*>* CServer::GetWorkQueue()
{
	return m_pWorkQueue;
}

bool CServer::KeepAlive()
{
	TPacket _packetToSend;
	int iSizeOfAdd = sizeof(m_ClientAddress);
	int _iNumOfBytesReceived;
	int _iPacketSize;

	//Make a thread local buffer to receive data into
	char _buffer[MAX_MESSAGE_LENGTH];

	while (m_bOnline)
	{
		_packetToSend.Serialize(KEEPALIVE, "");
		BroadcastData(_packetToSend.PacketData);
		std::this_thread::sleep_for(std::chrono::seconds(2));
		//std::this_thread::yield();

	} //End of while (true)
	return true;
}