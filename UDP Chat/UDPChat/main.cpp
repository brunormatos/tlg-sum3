/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: main.cpp
Description	: A multi-threaded chat application that can be run as a UDP chat client or UDP chat server
Author		: Bruno Matos
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/

//Library Includes
#include <Windows.h>
#include <cassert>
//#include <vld.h>
#include <thread>

//Local Includes
#include "consoletools.h"
#include "network.h"
#include "client.h"
#include "server.h"
#include "InputLineBuffer.h"
#include <functional>

// make sure the winsock lib is included...
#pragma comment(lib,"ws2_32.lib")

std::vector<std::thread *> g_vecThreads;


BOOL WINAPI ConsoleHandler(DWORD dwCtrlEvent)
{
	switch (dwCtrlEvent)
	{
	case CTRL_C_EVENT:
	case CTRL_CLOSE_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
	{
		myLog("New termination.\n");
		/*for (int i = 0; i < g_vecThreads.size(); i++)
		{
			if (g_vecThreads[i]->joinable()) g_vecThreads[i]->join();
			delete g_vecThreads[i];
		}*/

		//Shut Down the Network
		CNetwork& _rNetwork = CNetwork::GetInstance();
		_rNetwork.ShutDown();
		_rNetwork.DestroyInstance();

		delete CLogger::GetInstance();
		//delete[] _pcPacketData;
		myLog("Finished new termination.\n");
		return TRUE;
	}

	case CTRL_BREAK_EVENT:
	default:
		return FALSE;
	}
}

int main()
{
	unsigned char _ucChoice;
	_ucChoice = QueryOption("Do you want to save a log file (Y/N)?", "YN");
	switch (_ucChoice)
	{
	case 'Y':
	{
		CLogger::GetInstance()->SetLoggerAvailability(true);
		break;
	}
	case 'N':
	{
		CLogger::GetInstance()->SetLoggerAvailability(false);
		break;
	}
	default:
	{
		std::cout << "This is not a valid option\n";
		break;
	}
	}
	std::string _strFilenameTime = CLogger::getCurrentDateTime("now");
	std::replace(_strFilenameTime.begin(), _strFilenameTime.end(), ':', '-');
	std::replace(_strFilenameTime.begin(), _strFilenameTime.end(), '_', '-');
	std::replace(_strFilenameTime.begin(), _strFilenameTime.end(), ' ', '_');
	_strFilenameTime = "UDPChat_Log_" + _strFilenameTime;
	myLogOpen(_strFilenameTime, "Started application");
	char* _pcPacketData = 0; //A local buffer to receive packet data info
	_pcPacketData = new char[MAX_MESSAGE_LENGTH];
	strcpy_s(_pcPacketData, strlen("") + 1, "");

	char _cIPAddress[MAX_ADDRESS_LENGTH]; // An array to hold the IP Address as a string

	EEntityType _eNetworkEntityType;
	CInputLineBuffer _InputBuffer(MAX_MESSAGE_LENGTH);
	std::thread* _ClientReceiveThread = nullptr;
	std::thread* _ServerReceiveThread = nullptr;
	std::thread* _ServerKeepAliveThread = nullptr;

	//Set the console window event handler
	if (SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler, TRUE) == FALSE)
	{
		// unable to install handler... 
		// display message to the user
		myLog("Unable to install handler!\n");
	}

	//Get the instance of the network
	CNetwork& _rNetwork = CNetwork::GetInstance();
	_rNetwork.StartUp();

	//A pointer to hold a client instance
	CClient* _pClient = nullptr;
	//A pointer to hold a server instance
	CServer* _pServer = nullptr;

	// query, is this to be a client or a server?
	_ucChoice = QueryOption("Do you want to run a client or server (C/S)?", "CS");
	switch (_ucChoice)
	{
	case 'C':
	{
		_eNetworkEntityType = CLIENT;
		break;
	}
	case 'S':
	{
		_eNetworkEntityType = SERVER;
		break;
	}
	default:
	{
		myLog("This is not a valid option\n");
		unsigned int vectorSize = g_vecThreads.size();
		for (unsigned int i = 0; i < vectorSize; i++)
		{
			if (g_vecThreads[i]->joinable()) g_vecThreads[i]->join();
			delete g_vecThreads[i];
		}

		//Shut Down the Network
		_rNetwork.ShutDown();
		_rNetwork.DestroyInstance();

		delete CLogger::GetInstance();
		delete[] _pcPacketData;
		return 0;
		break;
	}
	}
	if (!_rNetwork.GetInstance().Initialise(_eNetworkEntityType))
	{
		myLog("Unable to initialise the Network........Press any key to continue......");
		_getch();
		for (unsigned int i = 0; i < g_vecThreads.size(); i++)
		{
			if (g_vecThreads[i]->joinable()) g_vecThreads[i]->join();
		}

		//Shut Down the Network
		_rNetwork.ShutDown();
		_rNetwork.DestroyInstance();

		delete CLogger::GetInstance();
		delete[] _pcPacketData;
		return 0;
	}

	//Run receive on a separate thread so that it does not block the main client thread.
	if (_eNetworkEntityType == CLIENT) //if network entity is a client
	{
		myLog("Creating CLIENT\n");
		_pClient = static_cast<CClient*>(_rNetwork.GetInstance().GetNetworkEntity());
		_ClientReceiveThread = new std::thread(&CClient::ReceiveData, _pClient, std::ref(_pcPacketData));
		g_vecThreads.push_back(_ClientReceiveThread);
	}

	//Run receive of server also on a separate thread 
	else if (_eNetworkEntityType == SERVER) //if network entity is a server
	{
		myLog("Creating SERVER\n");
		_pServer = static_cast<CServer*>(_rNetwork.GetInstance().GetNetworkEntity());
		_ServerReceiveThread = new std::thread(&CServer::ReceiveData, _pServer, std::ref(_pcPacketData));
		_ServerKeepAliveThread = new std::thread(&CServer::KeepAlive, _pServer);
		g_vecThreads.push_back(_ServerReceiveThread);
		g_vecThreads.push_back(_ServerKeepAliveThread);

	}

	while (_rNetwork.IsOnline())
	{
		if (_eNetworkEntityType == CLIENT) //if network entity is a client
		{
			_pClient = static_cast<CClient*>(_rNetwork.GetInstance().GetNetworkEntity());

			//Prepare for reading input from the user
			_InputBuffer.PrintToScreenTop();

			//Get input from the user
			if (_InputBuffer.Update())
			{
				ECommandType command = _InputBuffer.IsACommand();
				if (command != ECommandType::NONE)
				{
					TPacket _packet;
					switch (command)
					{
					case ECommandType::HELP:
						std::cout << "List of Commands:" << std::endl;
						std::cout << "[!?] - displays a list with all commands available" << std::endl;
						std::cout << "[!q] - quits the application" << std::endl;
						std::cout << "[!!] - bang" << std::endl;
						std::cout << "[!u] - displays a list with all connected users" << std::endl;
						std::cout << "[!s {user_name} {your message goes here}] - send secret message to user" << std::endl;
						break;
					case ECommandType::QUIT:
						_packet.Serialize(KILL, const_cast<char*>(""));
						_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);

						_pClient->GetWorkQueue()->empty();
						_rNetwork.GetInstance().ShutDown();
						break;
					case ECommandType::USER_LIST:
						_packet.Serialize(USERS, const_cast<char*>(""));
						_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);
						break;

					case ECommandType::SECRET:
						_packet.Serialize(PRIVATE, const_cast<char*>(std::string(_InputBuffer.GetString()).substr(3).c_str()));
						_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);
						break;
					case ECommandType::NONE:
					default:
						break;
					}

					//Clear the Input Buffer
					_InputBuffer.ClearString();
					//Print To Screen Top
					_InputBuffer.PrintToScreenTop();
				}
				else
				{
					// we completed a message, lets send it:
					int _iMessageSize = static_cast<int>(strlen(_InputBuffer.GetString()));

					//Put the message into a packet structure
					TPacket _packet;
					_packet.Serialize(DATA, const_cast<char*>(_InputBuffer.GetString())); //Hardcoded username; change to name as taken in via user input.
					_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);
					//Clear the Input Buffer
					_InputBuffer.ClearString();
					//Print To Screen Top
					//_InputBuffer.PrintToScreenTop();
				}
			}
			if (_pClient != nullptr)
			{
				//If the message queue is empty 
				if (_pClient->GetWorkQueue()->empty())
				{
					//Don't do anything
				}
				else
				{
					//Retrieve off a message from the queue and process it
					std::string temp;
					_pClient->GetWorkQueue()->pop(temp);
					_pClient->ProcessData(const_cast<char*>(temp.c_str()));
				}
			}

		}
		else //if you are running a server instance
		{
			//Prepare for reading input from the user
			_InputBuffer.PrintToScreenTop();

			//Get input from the user
			if (_InputBuffer.Update())
			{
				ECommandType command = _InputBuffer.IsACommand();
				if (command != ECommandType::NONE)
				{
					//TPacket _packet;
					switch (command)
					{
					case ECommandType::HELP:
						std::cout << "List of Commands:" << std::endl;
						std::cout << "[!?] - displays a list with all commands available" << std::endl;
						std::cout << "[!q] - quits the application" << std::endl;
						std::cout << "[!!] - bang" << std::endl;
						std::cout << "[!u] - displays a list with all connected users" << std::endl;
						std::cout << "[!s {user_name} {your message goes here}] - send secret message to user" << std::endl;
						break;
					case ECommandType::QUIT:
						//_packet.Serialize(KILL, const_cast<char*>(""));
						//_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);

						_pServer->GetWorkQueue()->empty();
						_rNetwork.GetInstance().ShutDown();
						break;
					case ECommandType::USER_LIST:
						//_packet.Serialize(USERS, const_cast<char*>(""));
						//_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);
						break;
					case ECommandType::NONE:
					default:
						break;
					}

					//Clear the Input Buffer
					_InputBuffer.ClearString();
					//Print To Screen Top
					_InputBuffer.PrintToScreenTop();
				}
				else
				{
					// we completed a message, lets send it:
					int _iMessageSize = static_cast<int>(strlen(_InputBuffer.GetString()));

					//Put the message into a packet structure
					TPacket _packet;
					_packet.Serialize(DATA, const_cast<char*>(_InputBuffer.GetString())); //Hardcoded username; change to name as taken in via user input.
					_rNetwork.GetInstance().GetNetworkEntity()->SendData(_packet.PacketData);
					//Clear the Input Buffer
					_InputBuffer.ClearString();
					//Print To Screen Top
					//_InputBuffer.PrintToScreenTop();
				}
			}
			if (_pServer != nullptr)
			{
				if (!_pServer->GetWorkQueue()->empty())
				{
					_rNetwork.GetInstance().GetNetworkEntity()->GetRemoteIPAddress(_cIPAddress);
					//std::cout << _cIPAddress
					//<< ":" << _rNetwork.GetInstance().GetNetworkEntity()->GetRemotePort() << "> " << _pcPacketData << std::endl;

					//Retrieve off a message from the queue and process it
					_pServer->GetWorkQueue()->pop(_pcPacketData);
					_pServer->ProcessData(_pcPacketData);
				}
			}
		}


	} //End of while network is Online

	for (unsigned int i = 0; i < g_vecThreads.size(); i++)
	{
		if (g_vecThreads[i]->joinable()) g_vecThreads[i]->join();
		delete g_vecThreads[i];
	}

	//Shut Down the Network
	_rNetwork.ShutDown();
	_rNetwork.DestroyInstance();

	delete CLogger::GetInstance();
	delete[] _pcPacketData;
	return 0;
}