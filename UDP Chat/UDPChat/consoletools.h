/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: consoletools.h
Description	: class prototype for console input tools (provided by the lecturer)
Author		: Asma Shakil
********************/

#pragma  once
#ifndef __CONSOLE_TOOLS_H__
#define __CONSOLE_TOOLS_H__

#include <iostream>
#include <ctype.h>
#include <conio.h>
#include <Windows.h>

	char* GetLineFromConsole(char* pBuffer, int iNumChars);

	//namespace utility
	//{
		template <size_t iNumChars>
		inline char* GetLineFromConsole(char(&pBuffer)[iNumChars])
		{
			return GetLineFromConsole(pBuffer, (int)iNumChars);
		}
	//}

	char QueryOption(const char* Question, const char* Accepted, bool bCaseSensitive = false);

	char* CollapseBackspacesAndCleanInput(char* pBuffer);

	unsigned short QueryPortNumber(unsigned short uDefault = 0);




#endif // __CONSOLE_TOOLS_H__
