/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: logger.h
Description	: a class prototype for creating a Log file
Author		: Bruno Matos
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/

#pragma once
#ifndef LOGGER_H_INCLUDED
#define LOGGER_H_INCLUDED

#pragma once

//library includes
#include <string>

#define myLogOpen(nameFile, s) CLogger::Logger(nameFile, s);
#define myLog(s) CLogger::Logger(s);


class CLogger
{
public:
	static std::string getCurrentDateTime(std::string);
	static void Logger(std::string, std::string);
	static void Logger(std::string);
	void SetLoggerAvailability(bool _value) { m_bIsAvailable = _value; };

	CLogger();
	~CLogger() { m_pInstance = nullptr; }

	static CLogger* GetInstance()
	{
		if (m_pInstance == nullptr)
			m_pInstance = new CLogger();
		return m_pInstance;
	}

private:
	static CLogger* m_pInstance;
	std::string m_sFilePath;
	bool m_bIsAvailable;
};


#endif //LOGGER_H_INCLUDED