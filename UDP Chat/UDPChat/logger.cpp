/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: logger.cpp
Description	: the class source for creating a Log file
Author		: Bruno Matos
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/

#include <time.h>
#include <fstream>      // std::ofstream
#include <iostream>

#include "logger.h"

CLogger* CLogger::m_pInstance = nullptr;

std::string CLogger::getCurrentDateTime(std::string s)
{
	using namespace std;
	time_t now = time(0);
	struct tm  tstruct;
	char  buf[80];
	localtime_s(&tstruct, &now);
	if (s == "now")
		strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
	else if (s == "date")
		strftime(buf, sizeof(buf), "%Y-%m-%d", &tstruct);
	return string(buf);
}

void CLogger::Logger(std::string fileName, std::string logMsg)
{
	CLogger* logger = GetInstance();
	using namespace std;
	string now = "";
	string filePath = "";
	if (logger->m_sFilePath == "")
	{
		now = getCurrentDateTime("now");
		filePath = fileName + ".txt";
		logger->m_sFilePath = filePath;
		cout << now << ":\t" << logMsg << '\n';
		if (logger->m_bIsAvailable)
		{
			ofstream ofs(filePath.c_str(), std::ios_base::out | std::ios_base::app);
			ofs << now << ":\t" << logMsg << '\n';
			ofs.close();
		}
	}
}

void CLogger::Logger(std::string logMsg)
{
	CLogger* logger = GetInstance();
	using namespace std;
	string now = "";
	string filePath = logger->m_sFilePath;
	if (filePath != "")
	{
			now = getCurrentDateTime("now");
			cout << now << ":\t" << logMsg;
		if(logger->m_bIsAvailable)
		{
			ofstream ofs(filePath.c_str(), std::ios_base::out | std::ios_base::app);
			ofs << now << ":\t" << logMsg;
			ofs.close();
		}
	}
}

CLogger::CLogger()
	: m_sFilePath(""),
	m_bIsAvailable(true)
{
}
