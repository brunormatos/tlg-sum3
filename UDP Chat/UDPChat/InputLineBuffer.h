/***********************
Bachelor of Software Engineering
Media Design School
Auckland
New Zealand

(c) 2018 Media Design School

File Name	: InputLineBuffer.h
Description	: class prototype for stylizing the Windows console window
Author		: Bruno Matos (based on template provided by the lecturer)
Mail		: Bruno.Mat8026@mediadesign.school.nz
********************/
#pragma once
#ifndef __INPUTLINEBUFFER_H__
#define __INPUTLINEBUFFER_H__

enum ECommandType : unsigned short
{
	BANG,
	QUIT,
	HELP,
	USER_LIST,
	NONE,
	SECRET
};

class CInputLineBuffer
{
public:
	CInputLineBuffer(unsigned int uBufferSize);
	~CInputLineBuffer();
	
	void ClearString();
	bool Update();
	void PrintToScreenTop();
	const char* GetString() const;
	ECommandType IsACommand();

protected:
	unsigned int	m_uBufferSize;	//!< The total size of the buffer.
	unsigned int	m_uBufferPos;	//!< The position of the next char in the buffer to be entered by the user.
	char*			m_pBuffer;		//!< The buffer contents.
};

#endif	//__INPUTLINEBUFFER_H__